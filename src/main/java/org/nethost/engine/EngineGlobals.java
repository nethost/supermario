package org.nethost.engine;

import java.awt.*;
import java.awt.image.BufferedImage;

public class EngineGlobals {
    public static BufferedImage backBuffer;
    public static MainGUI mainGUI;
    public static Insets insets;
}
