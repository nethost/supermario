package org.nethost.engine;

import org.nethost.engine.graphics.Graphics;
import org.nethost.engine.logic.MainLogic;

import javax.swing.*;
import java.awt.image.BufferedImage;

public class MainGUI extends JFrame {
    boolean isRunning = true;

    public void run() {
        initialize();

        while(isRunning)
        {
            long time = System.currentTimeMillis();

            MainLogic.update();
            Graphics.draw();

            time = (1000 / Settings.fps) - (System.currentTimeMillis() - time);

            if (time > 0)
            {
                try
                {
                    Thread.sleep(time);
                }
                catch(Exception e){}
            }
            setTitle("FPS: " + Settings.fps);
        }

        setVisible(false);
    }

    public void initialize() {
        setSize(Settings.windowWidth, Settings.windowHeight);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

        EngineGlobals.insets = this.getInsets();

        setSize(EngineGlobals.insets.left + Settings.windowWidth + EngineGlobals.insets.right,
                EngineGlobals.insets.top + Settings.windowHeight + EngineGlobals.insets.bottom);

        EngineGlobals.backBuffer = new BufferedImage(Settings.windowWidth, Settings.windowHeight, BufferedImage.TYPE_INT_RGB);

        Graphics.init();
        MainLogic.init();
    }
}
