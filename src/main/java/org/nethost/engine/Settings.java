package org.nethost.engine;

public class Settings {
    public static int fps = 60;
    public static int windowWidth = 512 *2;
    public static int windowHeight = 480*2;
    public static int imageScale = 2*2;
}
