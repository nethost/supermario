package org.nethost.engine.object;

import org.nethost.helpers.Vector2;
import org.nethost.sprites.ISprite;

import java.util.HashMap;
import java.util.Map;

public class GameObject {
    private Vector2 position;
    private boolean solid;
    private boolean kinematic;
    private HashMap<Integer, HashMap<Integer, ISprite>> sprites;

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public boolean isSolid() {
        return solid;
    }

    public void setSolid(boolean solid) {
        this.solid = solid;
    }

    public boolean isKinematic() {
        return kinematic;
    }

    public void setKinematic(boolean kinematic) {
        this.kinematic = kinematic;
    }

    public HashMap<Integer, HashMap<Integer, ISprite>> getSprites() {
        return sprites;
    }

    public void setSprites(HashMap<Integer, HashMap<Integer, ISprite>> sprites) {
        this.sprites = sprites;
    }

    public void draw() {
        for(Map.Entry<Integer, HashMap<Integer, ISprite>> entry : sprites.entrySet()) {
            int y = entry.getKey();
            HashMap<Integer, ISprite> column = entry.getValue();

            for(Map.Entry<Integer, ISprite> columnEntry : column.entrySet()) {
                int x = columnEntry.getKey();

                int xCoord = (x * 16) + this.getPosition().getX();
                int yCoord = (y * 16) + this.getPosition().getY();

                columnEntry.getValue().getSprite(xCoord, yCoord).drawSprite();
            }
        }
    }
}
