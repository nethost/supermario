package org.nethost.engine.sprite;

import org.nethost.engine.EngineGlobals;
import org.nethost.engine.Settings;
import org.nethost.engine.graphics.Graphics;

import java.awt.image.BufferedImage;

public class Sprite {
    private BufferedImage img;
    private int x,y;

    public Sprite(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public BufferedImage getImg() {
        return img;
    }

    public void setImg(BufferedImage img) {
        this.img = img;
    }

    public void drawSprite() {
        img = Graphics.scale(img, 16* Settings.imageScale, 16* Settings.imageScale);
        EngineGlobals.backBuffer.getGraphics().drawImage(img, this.x, this.y, null);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
