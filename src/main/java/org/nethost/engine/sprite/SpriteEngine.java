package org.nethost.engine.sprite;

import org.nethost.engine.Settings;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class SpriteEngine {
    BufferedImage tileSet = null;

    final int width = 16;
    final int height = 16;
    final int rows = 24;
    final int cols = 26;

    BufferedImage[] sprites = new BufferedImage[rows * cols];


    public SpriteEngine() {
        try {
            tileSet = ImageIO.read(new File("resources/objects1.png"));
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    sprites[(i * cols) + j] = tileSet.getSubimage(
                            j * width,
                            i * height,
                            width,
                            height
                    );
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Sprite getSprite(int index, int x, int y) {
        Sprite s = new Sprite(x* Settings.imageScale, y* Settings.imageScale);
        s.setImg(sprites[index]);

        return s;
    }
}

