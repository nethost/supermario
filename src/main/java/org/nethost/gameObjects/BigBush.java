package org.nethost.gameObjects;

import org.nethost.engine.object.GameObject;
import org.nethost.sprites.BaseSprite;
import org.nethost.sprites.ISprite;

import java.util.HashMap;

public class BigBush extends GameObject {
    public BigBush() {
        HashMap<Integer, HashMap<Integer, ISprite>> sprites = new HashMap<>();

        HashMap<Integer, ISprite> column = new HashMap<>();
        column.put(0, new BaseSprite(60));
        column.put(1, new BaseSprite(60));
        column.put(2, new BaseSprite(62));
        column.put(3, new BaseSprite(60));
        column.put(4, new BaseSprite(60));
        sprites.put(0, column);

        column = new HashMap<>();
        column.put(0, new BaseSprite(60));
        column.put(1, new BaseSprite(61));
        column.put(2, new BaseSprite(87));
        column.put(3, new BaseSprite(63));
        column.put(4, new BaseSprite(60));
        sprites.put(1, column);

        column = new HashMap<>();
        column.put(0, new BaseSprite(61));
        column.put(1, new BaseSprite(87));
        column.put(2, new BaseSprite(88));
        column.put(3, new BaseSprite(89));
        column.put(4, new BaseSprite(63));
        sprites.put(2, column);

        this.setSprites(sprites);
    }
}
