package org.nethost.gui;

import org.nethost.engine.EngineGlobals;
import org.nethost.engine.MainGUI;

public class Startup {

    public static void main(String[] args) {
        EngineGlobals.mainGUI = new MainGUI();

        EngineGlobals.mainGUI.run();

        System.exit(0);
    }


}
