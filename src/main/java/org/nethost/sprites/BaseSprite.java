package org.nethost.sprites;

import org.nethost.engine.sprite.Sprite;
import org.nethost.engine.sprite.SpriteEngine;

public class BaseSprite extends SpriteEngine implements ISprite {
    int index;

    public BaseSprite(int index) {
        this.index = index;
    }

    @Override
    public Sprite getSprite(int x, int y) {
        return super.getSprite(index, x, y);
    }
}
